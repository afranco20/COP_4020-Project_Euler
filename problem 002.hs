import System.IO

main = do
  -- putStrLn $ show solution_1
  putStrLn $ show solution_2

-- Brute Force
solution_1 = do
  sum [fib x | x <- [1..4000000], fib x < 4000000 && even (fib x)]
  where
    -- Binet's formula
    -- See https://wiki.haskell.org/The_Fibonacci_sequence#Constant-time_implementations
    fib x = round $ phi ** fromIntegral x / sq5
      where
        sq5 = sqrt 5 :: Double
        phi = (1 + sq5) / 2

-- More Elegant Solution
solution_2 = do
  sum [x | x <- takeWhile (<= 4000000) fibs, even x]
    where
      fibs = 1 : 1 : zipWith (+) fibs (tail fibs)
