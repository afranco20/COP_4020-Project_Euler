import System.IO
import Data.List

main = do
  putStrLn $ show solution_1
  -- putStrLn $ show solution_2

solution_1 = do
  maximum [x * y | x <- [100..999], y <- [100..999], reverse (show (x * y)) == show (x * y)]

solution_2 = do
  last $ sort [x * y | x <- [100..999], y <- [100..999], reverse (show (x * y)) == show (x * y)]
