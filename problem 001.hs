import System.IO
import Data.List

main = do
  putStrLn $ show solution_1
  -- putStrLn $ show solution_2

solution_1 = do
  sum [x | x <- [1..999], mod x 3 == 0 || mod x 5 == 0]

solution_2 = do
  sum (union [3,6..999] [5,10..999])
