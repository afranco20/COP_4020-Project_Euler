# COP 4020 - Project Euler
This repo is a list of solutions to project euler problems as practice for COP 4020.

Answers for solutions are verified against: https://github.com/nayuki/Project-Euler-solutions/blob/master/Answers.txt
