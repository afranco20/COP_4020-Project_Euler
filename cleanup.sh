#!/bin/bash

# a barebone script to cleanup ghc output files

for file in ./*.hs
do
  rm -f "${file:2:-3}.hi"
  rm -f "${file:2:-3}.o"
  rm -f "${file:2:-3}"
done
